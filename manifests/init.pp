#This class module is responsible for handling loging onto lysator's
# systems and providing a home directory (if one should be provided)
# $roots_only only allows people in the netgroup root
# $has_homedirs sets if people should have their own homedirs
# (/home/$user), or if everyone share /roots.
class lyslogin
(
  $roots_only = true,
  $has_homedirs = false,
) {
  # class { '::lyslogin::nis':
  #   roots_only   => $roots_only,
  #   has_homedirs => $has_homedirs,
  # }
  include ::lyslogin::kerberos
}
