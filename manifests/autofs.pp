class lyslogin::autofs {
  file { ['/home', '/mp']:
    ensure => directory,
  }

  file { '/lysator':
    ensure => link,
    target => '/mp/lysator',
  }

  file { '/var/mail':
    ensure => link,
    target => '/mp/mail',
  }

  # Having /lysator directly in the map would be nice, but doesn't
  # work. The actual /lysator is a submount under /mp
  # 
  # Lysators older config also included /pkg. But that seems to have
  # fallen out of many many years ago.

  package { ['autofs']:
    ensure => installed,
  }
  -> file { '/etc/auto.master':
    content => @(EOF)
    /home	yp:auto_home	nosuid,nodev,sec=sys,sec=krb5:krb5i:krb5p
    # /lysator	yp:auto_lysator	nosuid,nodev,sec=sys,sec=krb5:krb5i:krb5p
    /mp	yp:auto_lysator	nosuid,nodev,sec=sys,sec=krb5:krb5i:krb5p
    | EOF
  }
  -> service { 'autofs':
    ensure => running,
  }

  file { '/etc/autofs.conf':
    content => @(EOF)
    [ autofs ]
    master_map_name = /etc/auto.master
    timeout = 300
    browse_mode = no
    mount_nfs_default_protocol = 4

    [ amd ]
    dismount_interval = 300
    | EOF
  }
}
