#This is handles nis on lysators system. Defaults to only giving roots access to a system, without any home directories.

class lyslogin::nis(
  $roots_only = true,
  $has_homedirs = false,
) {

  case $facts['os']['family'] {
    'RedHat': {
      #include ::rpcbind
      $package_name = 'ypbind'
      $service_name = 'ypbind'
    }
    'Suse': {
      #include ::rpcbind
      $package_name = 'ypbind'
      $service_name = 'ypbind'
    }
    'Debian': {
      #include ::rpcbind
      $package_name = 'nis'
      $service_name = 'nis'
    }
    default: {
      fail('The osfamily on this machine is not supported by this module.')
    }
  }

  package { $package_name:
    ensure => installed,
  }

  service{ $service_name:
    ensure    => running,
    name      => $service_name,
    enable    => true,
    pattern   => 'ypbind',
    require   => [Package[ $package_name ],  File['yp.conf']],
    subscribe => [ File['yp.conf'] ];
  }

  file { 'yp.conf':
    ensure  => file,
    name    => '/etc/yp.conf',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => Package[ $package_name ],
    source  => 'puppet:///modules/lyslogin/yp.conf';
  }

  if $facts['os']['family'] == 'RedHat' {
    file { '/etc/sysconfig/network':
      ensure  => file,
      name    => '/etc/sysconfig/network',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('lyslogin/sysconfig-network-CentOS.erb');
    }
  }
  elsif $facts['os']['family'] =~ /Suse|Debian/ {
    file { '/etc/defaultdomain':
      ensure  => file,
      name    => '/etc/defaultdomain',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => 'lysator';
    }
  }


  if ($has_homedirs and ! $roots_only) {
    include lyslogin::autofs
  }


  if $roots_only == true {
    file {
      '/etc/nsswitch.conf':
        ensure  => file,
        owner   => 'root',
        group   => 'root',
        mode    => '0444',
        source  => 'puppet:///modules/lyslogin/nsswitch.conf-limited',
        require => Service[ $service_name ];

      '/roots':
        ensure => directory,
        owner  => 'root',
        group  => 'root',
        mode   => '0770';
    }

    $passwd_line = $has_homedirs ? {
      true => '+@root:x:::::',
      false => '+@root:x::::/roots:',
    }

    file_line { 'passwd_root':
      ensure  => present,
      path    => '/etc/passwd',
      line    => $passwd_line,
      require => File['/etc/nsswitch.conf'];
    }

    ~> file_line { 'passwd_nouser':
      ensure  => present,
      path    => '/etc/passwd',
      line    => '+:x:::::/bin/nologin',
      require => File['/etc/nsswitch.conf'];
    }
  }
  else {
    file { '/etc/nsswitch.conf':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => 'puppet:///modules/lyslogin/nsswitch.conf-user',
      require => Service[ $service_name ];
    }
  }

}
