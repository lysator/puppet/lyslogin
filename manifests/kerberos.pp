#Kerberos set up
class lyslogin::kerberos {

  # Should we maybe change to os.family instead?

  $krb5 = $facts['os']['name'] ? {
    'Debian' => 'krb5-user',
    'Ubuntu' => 'krb5-user',
    'centos' => 'krb5-libs',
    'rocky'  => 'krb5-libs',
    'redhat'  => 'krb5-libs',
    'OpenSuSE' => 'krb5-client',
  }

  $krb5_conf = $facts['os']['name'] ? {
    'Debian' => '/etc/krb5.conf',
    'Ubuntu' => '/etc/krb5.conf',
    'centos' => '/etc/krb5.conf',
    'rocky'  => '/etc/krb5.conf',
    'redhat'  => '/etc/krb5.conf',
    'OpenSuSE' => '/etc/krb5.conf',
    default => '/etc/krb5/krb5.conf',
  }

  # It feels like a bad hack to remove the IPAddressDeny from our
  # systemd-logind service, but without it I get big problems with
  # timing out on login.
  if $facts['os']['name'] == 'Ubuntu' {
    file { '/etc/systemd/system/systemd-logind.service.d':
      ensure => 'directory',
    }
    -> file { '/etc/systemd/system/systemd-logind.service.d/override.conf':
      ensure  => 'file',
      content => @(EOF)
      [Service]
      IPAddressDeny=
      | EOF
    }
  }

  package {
    [ $krb5 ]:
      ensure => installed,
  }

  file {
    'krb5.conf':
      ensure  => file,
      name    => $krb5_conf,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      require => Package[$krb5],
      source  => 'puppet:///modules/lyslogin/krb5.conf';
  }

  include ::lyslogin::kerberos::pam::linux
}
