#Kerberos pam module for (at the moment) Debian and CentOS.
class lyslogin::kerberos::pam::linux
{
  # os.family?
  $pam_krb5 = $facts['os']['name'] ? {
    'Debian' => 'libpam-krb5',
    'Ubuntu' => 'libpam-krb5',
    'centos' => 'pam_krb5',
    'rocky' => 'pam_krb5',
    'redhat' => 'pam_krb5',
    'OpenSuSE' => 'pam_krb5',
  }

  package {
    [ $pam_krb5 ]:
      ensure => installed,
  }

  # os.family?
  case $facts['os']['name'] {
    'Debian', 'Ubuntu', 'OpenSuSE' : {
      include ::lyslogin::kerberos::pam::linux::debian
    }
    'Rocky', 'RedHat', 'CentOS' : {
      if ($facts['os']['release']['major'].scanf('%d')[0] >= 8) {
        # pam_krb5 is no longer in the repos, instead, pull our own.
        require ::profiles::lysator_repo
      }
      include ::lyslogin::kerberos::pam::linux::redhat
    }
    default: {
      fail('The os on this machine is not supported by this module.')
    }
  }
}
