# RedHat-family relevant stuff
class lyslogin::kerberos::pam::linux::redhat
{
  file {
    '/etc/pam.d/system-auth':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => "puppet:///modules/lyslogin/pam/system-auth-${$facts['os']['family']}",
      require =>[  File['krb5.conf'], Package[$lyslogin::kerberos::pam::linux::pam_krb5] ];
    '/etc/pam.d/password-auth':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => "puppet:///modules/lyslogin/pam/password-auth-${$facts['os']['family']}",
      require =>[  File['krb5.conf'], Package[$lyslogin::kerberos::pam::linux::pam_krb5] ];
    '/etc/pam.d/fingerprint-auth':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => "puppet:///modules/lyslogin/pam/fingerprint-auth-${facts['os']['family']}",
      require =>[  File['krb5.conf'], Package[$lyslogin::kerberos::pam::linux::pam_krb5] ];
  }

  file { '/etc/sysconfig/authconfig':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/lyslogin/authconfig',
  }
}
