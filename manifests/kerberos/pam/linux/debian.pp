#Debian relevant stuff
class lyslogin::kerberos::pam::linux::debian
{
  file {
    '/etc/pam.d/common-auth':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => "puppet:///modules/lyslogin/pam/common-auth-${facts['os']['name']}",
      require =>[  File['krb5.conf'], Package[$lyslogin::kerberos::pam::linux::pam_krb5] ];
    '/etc/pam.d/common-account':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => "puppet:///modules/lyslogin/pam/common-account-${facts['os']['name']}",
      require =>[  File['krb5.conf'], Package[$lyslogin::kerberos::pam::linux::pam_krb5] ];
    '/etc/pam.d/common-session':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => "puppet:///modules/lyslogin/pam/common-session-${facts['os']['name']}",
      require =>[  File['krb5.conf'], Package[$lyslogin::kerberos::pam::linux::pam_krb5] ];
    '/etc/pam.d/common-password':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => "puppet:///modules/lyslogin/pam/common-password-${facts['os']['name']}",
      require =>[  File['krb5.conf'], Package[$lyslogin::kerberos::pam::linux::pam_krb5] ];
  }
}
